Description
-----------
A Node.js REST client for [mailman-core](https://gitlab.com/mailman/mailman).

Installing
-----------
    $ npm install mailman-client
        
Testing
--------

    $ npm test
        
Generating docs
---------------

    $ npm run docgen
        
Code coverage
-------------
    
    $ npm run cover

Running JSHint
--------------

    $ npm run jshint
    
Quick start
----------

    > var Client = require('mailman-client');
    > var cli = new Client({endpoint : 'http://localhost:8001/3.0/'
                            'username' : 'restadmin',
                            'password' : 'restpass'
                            });
                            
    // Usage with a callback function
    > var cb = function(err,data) {
        if (err) {
            console.log(err);
        }
        else {
            console.log(data);
        }
    };
    > cli.getLists(cb);
    
    // Promises
    > cli.getLists().then(console.log)

Links
-----
- [API docs](http://black-perl.in/mailman-client-docs/)
- [Coverage report](http://black-perl.in/mailman-cli/)







   