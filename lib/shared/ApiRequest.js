'use strict';
/**
* @module mailman-client
* @submodule ApiRequest
*/

var BaseRequest = require('./BaseRequest');
var lodash = require('lodash');
var qs = require('qs');


/**
* ApiRequest extends BaseRequest with custom properties and methods
* 
* @class ApiRequest
* @constructor
* @extends BaseRequest
* @param {Object} options  A hash of options for configuring the BaseRequest instance
* @param {String} options.endpoint  The endpoint URI to request
* @param {String} [options.username]  A username for authenticating API requests
* @param {String} [options.password]  A password for authenticating API requests
*/
 function ApiRequest(options) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * The object containing the query parameters
    * 
    * @private
    * @type Object
    * @property _params
    * @default {}
    */
    this._params = {};
    
    /**
    * A boolean flag that is set when there are query parameters to be rendered 
    * 
    * @private
    * @type Boolean
    * @property _queryParams
    * @default false
    */
    this._queryParams = false;
 }


 ApiRequest.prototype = new BaseRequest();


 /**
 * Record query parameters to be rendered in the final URI
 * 
 * @method query
 * @param {String|Object} key The name of the query parameter or an object containing query parameters as key,val 
 *                            pairs repectively
 * @param {String|Array} [value] If the supplied key is a String, then value can be of type Array or String 
 * @return {Object} Returns the ApiRequest instance itself if the arguments are in the required form else throw an Error
 * @chainable
 */
ApiRequest.prototype.query = function(key,value) { 
    var that = this; 
    // key is supplied as String
    if ( lodash.isString(key) && (lodash.isArray(value) || lodash.isString(value)) ) {
        this._params[key] = value;
    }
    // key is supplied as Object
    else if ( lodash.isObject(key) && !value ) { 
        lodash.each(key,function(v,k) {
            if ( lodash.isArray(v) || lodash.isString(v) ) {
                that._params[k] = v;
            }
            else {
                throw new Error(
                    "Unsupported object supplied, keys should be of type String and Value should be of type String/Array."
                );
            }
        });
    }
    else {
        throw new Error(
            'Arguments not found in the required form.'
            );
    }

    // if no error, means query parameters are set for rendering
    this._queryParams = true;
    return this;
 };


/**
* Compose query string by using the recorded query parameters
*
* @method _renderQuery
* @private
* @return {String} Returns the query string to be appended in the final URI
*/
ApiRequest.prototype._renderQuery = function() {
    var queryParams = this._params;
    // sorting is done to probably cache the URIs
    var queryString = qs.stringify(this._params)
        .split( '&' )
        .sort()
        .join( '&' );

    return queryString;
};


module.exports = ApiRequest;