'use strict';
/**
* @module mailman-client
* @submodule Address
*/

var ApiRequest = require('./shared/ApiRequest.js');
var Preferences = require('./Preferences.js');
var utils = require('./utils');


/**
* Address extends ApiRequest to perform operations on the address resource
*
* @class Address
* @constructor
* @extends ApiRequest
* @param {Object} options A hash of options for the Address instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {Object} address A hash of address object containing the location of the resource on the server
 */
function Address(options,address) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get','post']
    */
    this._supportedMethods = ['get','post'];

    /**
    * Associated email address corresponding Address object
    *
    * @property email
    * @type String
    * @private
    */
    this.email = address.email;

    /**
    * Resource URL
    *
    * @property _url
    * @type String
    * @private
    */
    this._url = address.self_link;
    
    /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @property _template
    * @type String
    * @default ''
    */
    this._template = '';

    /**
    * Object containing request path variables used to assemble the request URI
    *
    * @property _path
    * @type Object
    * @private
    * @default {}
    */
    this._path = {};

    /**
    * Object containing the resource information, will be null if getInfo() is never called
    *
    * @private
    * @type {Object}
    * @default null
    * @property _info
    */
    this._info = null;

    /**
    * Keys of the _info object
    *
    * @private
    * @type Array
    * @property _keys
    * @default []
    */
    this._keys = [];

}


Address.prototype  = new ApiRequest();


/**
* Fetch the resource by making a HTTP GET request and calls the callback(if any), and provide a 
* promise to the Address object itself
* 
* @method getInfo
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Address object itself
* @return {Promise} Promise to the Address object itself
*/
Address.prototype.getInfo = function(callback) {
    this._template = this._url.split(this._options.endpoint)[1];
    if ( !this._info ) {
        var that = this;
        return that.get().then(function(data) {
                        that._info = data;
                        that._keys = Object.keys(that._info);
                        that._setAccessors();
                        return utils.promisify(that,callback);
                    }).catch(function(e) {
                        console.error(e);
                    });
    }
    else {
        return utils.promisify(this,callback);
    }
};


/**
* Set the accessors on the object for the resource information i.e _info object
*
* @method _setAccessors
* @private
*/
Address.prototype._setAccessors = function() {
    /**
    * Display name of the address
    *
    * @property display_name
    * @type String
    */

    /**
    * Registration timestamp of the address
    *
    * @property registered_on
    * @type String
    */

    /** 
    * Verification timestamp of the address
    *
    * @property verified_on
    * @type String
    */
    
    if ( this._keys.length !== 0 ) {
        this._keys.forEach( (function(key) {

            Object.defineProperty(this,key,{

                get : function() {
                    if ( key in this._info )
                        return this._info[key];
                    else 
                        throw new Error('KeyError : key not found in the address hashmap');
                },

                configurable : true
            });

        }).bind(this)

        );

        /**
        * Getter for the Preferences instance corresponding to the current Address instance
        * 
        * @property preferences
        * @type Preferences
        */
        Object.defineProperty(this,'preferences',{
            get : function() {
                var endpoint = '/addresses/' + this.email + '/preferences';
                return new Preferences(this._options,endpoint);
            },
            configurable : true
        });
    } 
    else {
        throw new Error('getInfo() should be called first');
    }
};


/**
* Verify the address and calls the callback(if any), and provide a 
* promise to the Address object itself
*
* @method verify
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Address object itself
* @return {Promise} Promise to the Address object itself
*/
Address.prototype.verify = function(callback) {
    var that = this;
    that._template = '/addresses/(:email)/verify';
    that._path.email = that.email;
    return that.post().then(function(data) {
                    that._info = null;
                    return that.getInfo(callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Verify the address and calls the callback(if any), and provide a 
* promise to the Address object itself
*
* @method unverify
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Address object itself
* @return {Promise} Promise to the Address object itself
*/
Address.prototype.unverify = function(callback) {
    var that = this;
    that._template = '/addresses/(:email)/unverify';
    that._path.email = that.email;
    return that.post().then(function(data) {
                    that._info = null;
                    return that.getInfo(callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* String representation of the Address object
* 
* @method toString
* @return {String} Associated email address to the Address object
*/
Address.prototype.toString = function() {
    return this.email;
};


module.exports = Address;