'use strict';
/*
    Client module
*/

var ApiRequest = require('./shared/ApiRequest.js');
var Preferences = require('./Preferences.js');
var User = require('./User.js');
var Domain = require('./Domain.js');
var Address = require('./Address.js');
var List = require('./List.js');
var Member = require('./Member.js');
var Page = require('./Page.js');
var utils = require('./utils');


/**
* Client extends ApiRequest to serve as the entry point for the Mailman Client API
* 
* @constructor
* @class Client
* @extends ApiRequest
* @param {Object} options A hash of options for the Client instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
*/
function Client(options) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    this._setAccessors();
}


Client.prototype = new ApiRequest();


/**
* Provides the system versions information
* 
* @async
* @method getVersions
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The system version information
* @return {Promise} Promise to the system version information
*/
Client.prototype.getVersions = function(callback) {
    this._template = '/system/versions';
    return this.get().then(function(resp) {
                    return utils.promisify(resp,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Set the accessors on the Client instance
*
* @private
* @method _setAccessors
*/
Client.prototype._setAccessors = function() {
    /**
    * Preferences object representing the system preferences
    *
    * @property preferences
    * @type Preferences
    */
    var that = this;
    Object.defineProperty(this,'preferences',{
        get : function() {
            return new Preferences(that._options,'/system/preferences');
        }
    });
};


/**
* Calls the callback(if any) or provide a promise to an array of List instances
* 
* @method getLists
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of List instances
* @return {Promise} Promise to an array of List instances
*/
Client.prototype.getLists = function(callback) {
    var that = this;
    that._template = '/lists';
    return that.get().then(function(resp) {
                    var items = [];
                    if ( 'entries' in resp ) {
                        resp.entries.forEach(function(entry) {
                            items.push(new List(that._options,entry.self_link));
                        });
                    }
                    return utils.promisify(items,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Provides a paging interface to the lists
*
* @method getListPage
* @async
* @param {Number} count Number of items on a page
* @param {Page} page Number of pages to be requested
* @param {Function} [callback] The callback function to be invoked with a Page instance
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result A Page instance
* @return {Promise} Promise to a Page instance
*/
Client.prototype.getListPage = function(count,page) {
    if ( typeof count === 'undefined' ) { count = 50; }
    if ( typeof page === 'undefined' ) { count = 1; }
    return new Page(this._options,'/lists',List,count,page);
};


/**
* Calls the callback(if any) or provide a promise to an array of Domain instances
* 
* @method getDomains
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of Domain instances
* @return {Promise} Promise to an array of Domain instances
*/
Client.prototype.getDomains = function(callback) {
    var that = this;
    that._template = '/domains';
    return that.get().then(function(resp) {
                    var items = [];
                    if ( 'entries' in resp ) {
                        resp.entries.forEach(function(entry) {
                            items.push(new Domain(that._options,entry.self_link));
                        });
                        items.sort(function(x,y) {
                            return (x > y) - (x < y);
                        });
                    }
                    return utils.promisify(items,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Calls the callback(if any) or provide a promise to an array of Member instances
* 
* @method getMembers
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of Members instances
* @return {Promise} Promise to an array of Member instances
*/
Client.prototype.getMembers = function(callback) {
    var that = this;
    that._template = '/members';
    return that.get().then(function(resp) {
                    var items = [];
                    if ( 'entries' in resp ) {
                        resp.entries.forEach(function(entry) {
                            items.push(new Member(that._options,entry.self_link));
                        });
                    }
                    return utils.promisify(items,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Provides a paging interface to the lists
*
* @method getMemberPage
* @async
* @param {Number} count Number of items on a page
* @param {Page} page Number of pages to be requested
* @param {Function} [callback] The callback function to be invoked with a Page instance
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result A Page instance
* @return {Promise} Promise to a Page instance
*/
Client.prototype.getMemberPage = function(count,page) {
    if ( typeof count === 'undefined' )     { count = 50; }
    if ( typeof page === 'undefined' )      { page = 1; }
    return new Page(this._options,'/members',Member,count,page);
};   


/**
* Calls the callback(if any) or provide a promise to an array of User instances
* 
* @method getUsers
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of User instances
* @return {Promise} Promise to an array of User instances
*/
Client.prototype.getUsers = function(callback) {
    var that = this;
    that._template = '/users';
    return that.get().then(function(resp) {
                    var items = [];
                    if ( 'entries' in resp ) {
                        resp.entries.forEach(function(entry) {
                            items.push(new User(that._options,entry.self_link));    
                        });
                    }
                    items.sort(function(x,y) {
                        return (x > y) - (x < y);
                    });
                    return utils.promisify(items,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Provides a paging interface to the users
*
* @method getUserPage
* @async
* @param {Number} count Number of items on a page
* @param {Page} page Number of pages to be requested
* @param {Function} [callback] The callback function to be invoked with a Page instance
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result A Page instance
* @return {Promise} Promise to a Page instance
*/
Client.prototype.getUserPage = function(count,page) {
    if ( typeof count === 'undefined' )     { count = 50; }
    if ( typeof page === 'undefined' )      { page = 1; }
    return new Page(this._options,'/users',User,count,page);
};


/**
* Create a domain and call the callback(if any) or provide a promise to the Domain instance
*
* @async
* @method createDomain
* @param {String} mailHost The mail host 
* @param {String} [baseURL] The base URL of the mail host
* @param {String} [description] Description about the domain
* @param {String} [owner] Email of the domain owner
* @param {Function} [callback] The callback function to be invoked with the created Domain instance
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The created Domain instance
* @return {Promise} Promise to the created Domain instance
*/
Client.prototype.createDomain = function(mailHost,baseURL,description,owner,callback) {
    var payload = { mail_host : mailHost };

    if ( typeof baseURL === 'undefined' )       { baseURL = null; } 
    else    { payload.base_url = baseURL; }

    if ( typeof description === 'undefined' )       { description = null; }
    else    { payload.description = description; }

    if ( typeof owner === 'undefined' )     { owner = null; }
    else    { payload.owner = owner; }

    var that = this;
    that._template = '/domains';
    return that.post(payload,null,'identity').then(function(resp) {
                    var domainObj = new Domain(that._options,resp.headers.location);
                    return utils.promisify(domainObj,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Delete a domain
*
* @async
* @method deleteDomain
* @param {Function} [callback] The callback function to be invoked 
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Client instance itself
* @return {Promise} Promise to the Client instance itself
*/
Client.prototype.deleteDomain = function(mailHost,callback) {
    var that = this;
    that._template = '/domains/' + mailHost;
    return that.delete().then(function() {
                    return utils.promisify(that,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Get a domain by it's web host name or mail host name, call the provided callback(if any)
* with the Domain instance or provide a promise to it
*
* @async
* @method getDomain
* @param {String} mailHost The mail host of the domain
* @param {String} webHost The web host of the domain
* @param {Function} [callback] The callback function to be invoked 
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Domain instance
* @return {Promise} Promise to the Domain instance
*/
Client.prototype.getDomain = function(mailHost,webHost,callback) {
    if ( typeof mailHost === 'undefined' )   { mailHost = null; }
    if ( typeof webHost === 'undefined' )   { webHost = null; }

    var that = this;
    if ( mailHost ) {
        that._template = '/domains/' + mailHost;
        return that.get().then(function(resp) {
                        return new Domain(that._options,resp.self_link);
                    }).then(function(domain) {
                        return domain.getInfo();
                    }).catch(function(e) {
                        console.error(e);
                    });
    }
    else if ( webHost ) {
        return new utils.Promise(function(resolve,reject) {
            that.getDomains().then(function(domains) {
                        var numDomains = domains.length;
                        var loopcount = 0;
                        domains.forEach(function(domain) {
                            domain.getInfo().then(function() {
                                loopcount++;
                                if ( domain.base_url === webHost ) {
                                    resolve(domain);
                                }
                                else if ( loopcount === numDomains ) {
                                    resolve(null);
                                }
                            }); // getInfo
                        }); // forEach
                    }).catch(function(e) {
                        console.error(e);
                    }); // getDomains
        }).nodeify(callback || utils.noop);
    }
};


/**
* Create a mailman user and call the callback(if any) with the User instance or provide a promise to it
*
* @async
* @method createUser 
* @param {String} email The email address of the user
* @param {String} password A password string
* @param {String} displayName The display name of the user
* @return {Promise} Returns a promise to the created User instance
*/
Client.prototype.createUser = function(email,password,displayName,callback) {
    var that = this;
    if ( typeof displayName === 'undefined' )       { displayName = ''; }
    that._template = '/users';
    var payload = {
        email : email,
        password : password,
        display_name : displayName
    };
    return that.post(payload,null,'identity').then(function(resp) {
                    var userObj = new User(that._options,resp.headers.location);
                    return utils.promisify(userObj,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};   


/**
* Get a user by it's associated email address and call the callback(if any) with the User instance or
* provide a promise to it
*
* @async
* @method getUser
* @param {String} email The email address of the user
* @param {Function} [callback] The callback function to be invoked with the created Domain instance
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The User instance
* @return {Promise} Promise to the User instance
*/
Client.prototype.getUser = function(email,callback) {
    var that = this;
    that._template = '/users/' + email;
    return that.get().then(function(resp) {
                    var userObj = new User(that._options,resp.self_link);
                    return utils.promisify(userObj,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Get an address by it's associated email address and call the callback(if any) with the Address instance or
* provide a promise to it
*
* @async
* @method getAddress
* @param {String} email The associated email address
* @param {Function} [callback] The callback function to be invoked 
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Address instance
* @return {Promise} Promise to the Address instance
*/
Client.prototype.getAddress = function(email,callback) {
    var that = this;
    that._template = '/addresses/' + email;
    return that.get().then(function(resp) {
                    var addressObj = new Address(that._options,resp);
                    return utils.promisify(addressObj,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Get a list by it's fqdn listname (fully qualified domain name) and call the callback(if any) 
* with the List instance or provide a promise to it
*
* @async
* @method getList
* @param {String} fqdnListname The fully qualified domian name of the list
* @param {Function} [callback] The callback function to be invoked 
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The List instance
* @return {Promise} Promise to the List instance
*/
Client.prototype.getList = function(fqdnListname,callback) {
    var that = this;
    that._template = '/lists/' + fqdnListname;
    return that.get().then(function(resp) {
                    var listObj = new List(that._options,resp.self_link);
                    return utils.promisify(listObj,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Delete a list by it's fqdn listname (fully qualified domain name) and call the callback(if any) 
* with the Client instance or provide a promise to it
*
* @async
* @method deleteList
* @param {String} fqdnListname The fully qualified domian name of the list
* @param {Function} [callback] The callback function to be invoked 
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The List instance
* @return {Promise} Promise to the List instance
*/
Client.prototype.deleteList = function(fqdnListname,callback) {
    var that = this;
    that._template = '/lists/' + fqdnListname;
    return that.delete().then(function(resp) {
                    return utils.promisify(that,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* String representation of the Client instance
*
* @method toString
* @return {String} Return the string representation of the Client instance
*/
Client.prototype.toString = function() {
    return '<Client (' + this._options.username + ':' + this._options.password + ') ' + this._options.endpoint + '>';
};


module.exports = Client;

