'use strict';
/**
* @module mailman-client
* @submodule Addresses
*/

var ApiRequest = require('./shared/ApiRequest.js');
var Address = require('./Address.js');
var utils = require('./utils');


/**
* Addresses extends ApiRequest to perform operations on the Addresses resource
*
* @class Addresses
* @constructor
* @extends ApiRequest
* @param {Object} options A hash of options for the Address instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {String} template Template of Addresses resource
* @param {String} userId ID of an user resource
*/
function Addresses(options,template,userId) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get']
    */
    this._supportedMethods = ['get'];

    /**
    * Object containing the resource information, will be empty if getAddresses() is never called
    *
    * @private
    * @type {Array}
    * @default []
    * @property _addresses
    */
    this._addresses = [];

    /**
    * Array containing the Address instances 
    *
    * @private
    * @type {Array}
    * @default []
    * @property _addressItems
    */
    this._addressItems = []; 

    /**
    * ID of an user resource, used for rendering the path
    *
    * @private
    * @type {Array}
    * @default []
    * @property _addressItems
    */
    this._userId = userId;

    /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @type String
    * @property _template
    */
    this._template = template || '/users/(:user)/addresses';

    /**
    * Object containing request path variables used to assemble the request URI
    *
    * @property _path
    * @type Object
    * @private
    */
    this._path = { user : this._userId };
}


Addresses.prototype = new ApiRequest();


/**
* Fetch the resource by making a HTTP GET request and calls the callback(if any), and provide a 
* promise to an array of Address instances
* 
* @method getAddresses
* @async
* @param {Function} [callback] The callback function to be invoked with an array of Address instances
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of Address instances
* @return {Promise} Promise to an array of Address instances
*/
Addresses.prototype.getAddresses = function(callback) {
    if ( this._addresses.length === 0 )
    {
        var that = this;
        return that.get().then(function(content) {
                        var items = [];
                        if ( 'entries' in content ) {
                            that._addresses = content['entries'];
                            that.length = that._addresses.length;
                        }
                        that._addresses.forEach(function(addrEntry) {
                            items.push(new Address(that._options,addrEntry));
                        });
                        that._addressItems = items;
                        return utils.promisify(that._addressItems,callback);
                    }).catch(function(e) {
                        // this should be passed to the node style callback when there is error and callback
                        console.error(e);
                    });
    } 
    else {
        return utils.promisify(this._addressItems,callback);
    }
};


/**
* Iterate over each of the Address instance associated with this Addresses instance
* 
* @method each 
* @param {Function} callback A callback function to be called with each Address instance
*/
Addresses.prototype.each = function(callback) {
    var that = this;
    that.getAddresses().then(function() {
        for ( var index = 0 ; index < that.length ; index++ ) {
            callback(new Address(that._options,that._addresses[index]));
        }
    });
};


/**
* Filter the Address instances by an email address and calls the callback(if any) with it, and provide a 
* Promise to it
* 
* @method each 
* @async
* @param {String} email Email address upon which filtering is done
* @param {Function} [callback] A callback function to be called with the filtered Address instance
* @return {Promise} A promise to the filtered Address instance 
*/
Addresses.prototype.filterByEmail = function(email,callback) {
    if ( typeof email === 'undefined' )     { throw new Error('email not provided'); }
    var filterCallback = function(add) {
        if ( add.email === email )
            return true;
    }; 
    var that = this;
    return that.getAddresses().then(function() {
                   var filteredArray = that._addressItems.filter(filterCallback);
                   if ( filteredArray.length !== 0 )
                       return utils.promisify(filteredArray[0],callback);
                   else 
                       return utils.promisify(null,callback);
               });
};


module.exports = Addresses;