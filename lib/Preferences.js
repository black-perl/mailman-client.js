'use strict';
/**
* @module mailman-client
* @submodule Preferences
*/

var ApiRequest = require('./shared/ApiRequest');
var utils = require('./utils');
var lodash = require('lodash');

var PREFERENCE_FIELDS = [
    'acknowledge_posts',
    'delivery_mode',
    'delivery_status',
    'hide_address',
    'preferred_language',
    'receive_list_copy',
    'receive_own_postings',
    ];
var PREF_READ_ONLY_ATTRS = [
    'http_etag',
    'self_link',
    ];


/**
* Preferences extends ApiRequest to do operations on the preferences resource
*
* @constructor
* @class Preferences
* @extends ApiRequest
* @param {Object} options A hash of options for the Settings instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {String} template The URI template used to generate the URI to request 
*/
function Preferences(options,template) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * Supported HTTP methods
    *
    * @property _supportedMethods
    * @type Array
    * @private
    * @default ['get','patch']
    */
    this._supportedMethods = ['get','patch'];

    /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @type String
    * @property _template
    */
    this._template = template;

   /**
    * Object containing the resource information, will be null if getInfo() is never called
    *
    * @private
    * @type {Object}
    * @default null
    * @property _preferences
    */
    this._preferences = null;

    this.delivery_mode = null;

    /**
    * Array of properties based on the Preferences _info object
    *
    * @private
    * @type Array
    * @property _keys
    * @default []
    */
    this._keys = [];

    /**
    * Length of the Preferences object
    *
    * @property length
    * @default 0
    */
    this.length = 0;
}   


Preferences.prototype = new ApiRequest();


/**
* Nice little helping function for setting the _preferences from the data fetched from the server
*
* @private
* @method _setPreferences
*/
Preferences.prototype._setPreferences = function(data) {
    this._preferences = {};
    lodash.extend(this._preferences,data);
    PREFERENCE_FIELDS.forEach( (function(key) {
            if (!(key in data)) {
                this._preferences[key] = null;
            }
        }).bind(this)
    );
};


/**
* Fetch the resource by making a HTTP GET request and calls the callback(if any), and provide a 
* promise to the Preferences object itself
* 
* @method getPreferences
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Preferences object itself
* @return {Promise} Promise to the Preferences object itself
*/
Preferences.prototype.getPreferences = function(callback) {
    if ( !this._preferences ) {
        var that = this;
        return that.get().then(function(data) {
                        that._setPreferences(data);
                        return that.keys();
                    }).then(function() {
                        that._setAccessors();
                        return utils.promisify(that,callback);
                    }).catch(function(e) {
                        console.error(e);
                    });
    }
    else {
        return utils.promisify(this,callback);
    }
};


/**
* Collect the keys of Preferences object in an array if _preferences is present
*
* @async
* @method keys
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of preferences fields
* @return {Promise} Promise to the array of preferences fields
*/
Preferences.prototype.keys = function(callback) {
    var that = this;
    return that.getPreferences().then(function() {
                    that._keys = Object.keys(that._preferences);
                    that.length = that._keys.length;
                    return utils.promisify(that._keys,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Set the accessors on the object for the resource information i.e _info object
*
* @method _setAccessors
* @private
*/
Preferences.prototype._setAccessors = function() {

    this._keys.forEach( (function(key) {

        Object.defineProperty(this,key,{

            get : function() {
                if ( key in this._preferences )
                    return this._preferences[key];
                else 
                    throw new Error('KeyError : key not found in the preferences hashmap');
            },

            set : function(value) {
                this._preferences[key] = value;
            },

            configurable : true
        });

    }).bind(this)

    );
};


/**
* Save the preferences upstream
*
* @async
* @method save
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The Preferences object itself
* @return {Promise} Promise to the Preferences object itself
*/
Preferences.prototype.save = function(callback) {
    var data = {};
    var that = this;
    that._keys.forEach(function(key) {

            if ( !( key in PREF_READ_ONLY_ATTRS ) && that._preferences[key] !== null ) {
                data[key] = that._preferences[key];
            }
            
        }
    );
    return that.patch(data).then(function(data) {
                    return utils.promisify(that,callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* String representation of the Preferences object
*
* @method toString
* @return {String} Returns the string representation of the Preferences object
*/
Preferences.prototype.toString = function() {
    return JSON.stringify(this._preferences);
};


module.exports = Preferences;

