'use strict';
/**
* @module mailman-client
* @submodule ListArchivers
*/

var ApiRequest = require('./shared/ApiRequest.js');
var utils = require('./utils');


/**
* ListArchivers extends ApiRequest to perform operation on the list archivers
* 
* @class ListArchivers
* @constructor
* @extends ApiRequest
* @param {Object} options A hash of options for the ListArchivers instance
* @param {String} options.endpoint The endpoint URI to request
* @param {String} [options.username] A username for authenticating API requests
* @param {String} [options.password] A password for authenticating API requests
* @param {String} listId ID of the corresponding list
*/
function ListArchivers(options,listId) {
    /**
    * Configuration options such as the endpoint of the API, auth parameters etc.
    * 
    * @private
    * @type Object
    * @property _options
    * @default {}
    */
    this._options = options || {};

    /**
    * ID of the corresponding list
    *
    * @private
    * @type String
    * @property _listId
    */ 
    this._listId = listId;

    /**
    * The URI template used to generate the URI to request 
    *
    * @private
    * @type String
    * @property _template
    */
    this._template = '/lists/(:listId)/archivers';

    /**
    * Object containing request path variables used to assemble the request URI
    *
    * @property _path
    * @type Object
    * @private
    */
    this._path = { listId : this._listId };

    /**
    * Object containing the resource information, will be null if getInfo() is never called
    *
    * @private
    * @type {Object}
    * @default null
    * @property _info
    */
    this._info = null;
}


ListArchivers.prototype = new ApiRequest();


/**
* Fetch the resource by making a HTTP GET request and calls the callback(if any), and provide a 
* promise to the ListArchivers object itself
* 
* @method getInfo
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The ListArchivers object itself
* @return {Promise} Promise to the ListArchivers object itself
*/
ListArchivers.prototype.getInfo = function(callback) {
    if ( !this._info) {
        var that = this;
        return that.get().then(function(data) {
                    that._info = data;
                    // delete non required keys
                    delete that._info['http_etag'];
                    return utils.promisify(that,callback);
                });
    } else {
        return utils.promisify(this._info,callback);
    }
};


/**
* Set the value of a list archiver
* 
* @method setArchiver
* @async
* @param {String} key Name of the list archiver
* @param {Boolean} value The state of the archiver to be set 
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The ListArchivers object itself
* @return {Promise} Promise to the ListArchivers object itself
*/
ListArchivers.prototype.setArchiver = function(key,value,callback) {
    if ( key && value ) {
        var that = this;
        return that.getInfo().then(function() {
                        // set the value
                        that._info[key] = value;
                        // make payload
                        var payload = that._info;
                        return that.put(payload);
                    }).then(function() {
                        return utils.promisify(that,callback);
                    }).catch(function(e) {
                        console.error(e);
                    });
    } else {
        throw new Error('Either key or value or both missing');
    }
};


/**
* Get the current state of the list archiver
*
* @method getArchiver
* @async
* @param {String} key The name of the list archiver
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result The state of the list archiver
* @return {Promise} Promise to the state of the list archiver
*/
ListArchivers.prototype.getArchiver = function(key,callback) {
    var that = this;
    return that.getInfo().then(function() {
                    return utils.promisify(that._info[key],callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/**
* Calls the callback(if any) with an array of available list archivers, and provide a promise to it
*
* @method all
* @async
* @param {Function} [callback] The callback function to be invoked with the results
* @param {Error|Object} callback.err Any errors encountered during the request
* @param {Object} callback.result An array of available list archivers
* @return {Promise} Promise to an array of available list archivers
*/
ListArchivers.prototype.all = function(callback) {
    var that = this;
    return that.getInfo().then(function() {
                    return utils.promisify(Object.keys(that._info),callback);
                }).catch(function(e) {
                    console.error(e);
                });
};


/** 
* Iterating interface to the ListArchivers instance, callback is called each time with a list archiver and it's value
* 
* @method each
* @async
* @param {Function} callback The callback function to be invoked
*/
ListArchivers.prototype.each = function(callback) {
    var that = this;
    return that.getInfo().then(function() {
                    return that.all();
                }).then(function(archivers) {
                    archivers.forEach(function(archiver) {
                        callback(archiver,that._info[archiver]);
                    });
                });
};


/**
* String representation of the ListArchivers instance
*
* @method toString
* @return {String} String representation of the ListArchivers instance
*/
ListArchivers.prototype.toString = function() {
    return '<Archivers on ' + this._listId + '>';
};


module.exports = ListArchivers;




