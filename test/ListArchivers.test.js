'use strict';

/** 
Unit tests for the ListArchivers module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var ListArchivers = require('../lib/ListArchivers.js');


//loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/listarchivers.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('ListArchivers',function() {
    var listArchiversInstance, options, responseData, archivers;

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    responseData = { 
        mhonarc: false,
        'mail-archive': false,
        http_etag: '"e8abfb237f8ebda71c95d59b907288473359bb5a"',
        prototype: false 
    };

    archivers = [
        'mhonarc',
        'mail-archive',
        'prototype'
    ];

    beforeEach(function() {
        listArchiversInstance = new ListArchivers(options,
                                                    'foolist.somedomain.org');
    });

    describe('constructor',function() {

        it('should be an instance of ApiRequest',function() {
            expect(listArchiversInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of BaseRequest',function() {
            expect(listArchiversInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of Member',function() {
            expect(listArchiversInstance).to.be.instanceof(ListArchivers);
        });

        it('should initialise the instance properties',function() {
            expect(listArchiversInstance._listId).to.be.equal('foolist.somedomain.org');
            expect(listArchiversInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/foolist.somedomain.org/archivers');
        });

    }); // constructor

    describe('.getInfo()',function(done) {

        it('should provide promise to the ListArchivers instance',function(done) {
            listArchiversInstance.getInfo().then(function(retValue) {
                expect(retValue).to.be.deep.equal(listArchiversInstance);
                var expectedInfo = {
                    mhonarc: false,
                    'mail-archive': false,
                    prototype: false 
                };
                expect(listArchiversInstance._info).to.deep.equal(expectedInfo);
                done();
            });
        });

    }); // .getInfo()

    describe('.setArchiver()',function(done) {

        it('should throw an error if both key and value are not supplied',function() {
            expect(listArchiversInstance.setArchiver.bind(listArchiversInstance,'foo','')).to.throw(Error);
            expect(listArchiversInstance.setArchiver.bind(listArchiversInstance,'','bar')).to.throw(Error);
        });

        it('should set the value of an archiver',function(done) {
            var httpMethodSpy = sinon.spy(listArchiversInstance,'put');
            listArchiversInstance.setArchiver('mhonarc',true).then(function(retValue) {
                expect(retValue).to.be.deep.equal(listArchiversInstance);
                expect(listArchiversInstance._info['mhonarc']).to.be.true;
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(listArchiversInstance._info);
                done();
            });
        });

    }); // .setArchiver

    describe('.getArchiver()',function(done) {

        it('should return the status of the archiver',function(done) {
            var loopCount = archivers.length;
            var counter = 0;
            archivers.forEach(function(archiver) {
                listArchiversInstance.getArchiver(archiver).then(function(retValue) {
                    expect(retValue).to.be.equal(listArchiversInstance._info[archiver]);
                    if ( ++counter == loopCount ) {
                        done();
                    }
                });
            });
        });

    });

    describe('.all()',function(done) {

        it('should return all the list archivers',function(done) {
            listArchiversInstance.all().then(function(retValue) {
                expect(retValue).to.be.deep.equal(archivers);
                done();
            });
        });

    }); // .all()

    describe('.each()',function(done) {

        it('should call the callback function with the listarchiver and it\'s value',function(done) {
            var callback = function(archiver,value) {
                expect(value).to.be.equal(listArchiversInstance._info[archiver]);
                if ( ++counter == loopCount ) {
                    done();
                }
            };
            var callbackSpy = sinon.spy(callback);
            var counter = 0, loopCount = archivers.length;
            listArchiversInstance.each(callbackSpy);
        }); 

    });

    describe('.toString()',function() {

        it('should have a string respresentation',function() {
            expect(listArchiversInstance.toString()).to.be.equal('<Archivers on foolist.somedomain.org>');
        });

    });

}); // ListArchivers






