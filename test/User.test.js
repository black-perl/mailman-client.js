'use strict';

/**
Unit tests for the User module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Preferences = require('../lib/Preferences.js');
var User = require('../lib/User.js');
var Addresses = require('../lib/Addresses.js');
var Member = require('../lib/Member.js');


// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/user.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('User',function() {
    var userInstance,options,responseData;

    options = {
                endpoint : 'http://localhost:8001/3.0',
                username : 'restadmin',
                password : 'restpass'
              };

    responseData = {
                    created_on : "2015-06-17T17:05:37.741005",
                    user_id : 149132891060733238619497360715436428427,
                    http_etag : "\"bdf233fb9367a8ea069142db3fd8fd24e201eead\"",
                    is_server_owner : false,
                    password : "$6$rounds=96671$qHl/Gpb.BrSqko8c$/mBbW.Iv4AGoxWvW95puHWkFTvD7UDl2DzrSQmQ7HlTe2FjUkKantkaJxiq0dfA4qrCIIeORGgeHGnfXN9GiO.",
                    display_name : "black-perl",
                    self_link : "http://localhost:8001/3.0/users/149132891060733238619497360715436428427"
                };

    beforeEach(function() {
        userInstance = new User(options,
                                'http://localhost:8001/3.0/users/149132891060733238619497360715436428427'
                               );
    });

    describe('constructor',function() {

        it('should be an instance of ApiRequest',function() {
            expect(userInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of BaseRequest',function() {
            expect(userInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of User',function() {
            expect(userInstance).to.be.instanceof(User);
        });

        it('should initialise the instance properties',function() {
            expect(userInstance._supportedMethods).to.deep.equal(['get','post','patch','delete']);
            expect(userInstance._url).to.be.equal('http://localhost:8001/3.0/users/149132891060733238619497360715436428427');
        });
    }); // constructor

    describe('.getInfo()',function(done) {

        it('should set the _template property',function(done) {
            userInstance.getInfo().then(function() {
                expect(userInstance._template).to.be.equal('/users/149132891060733238619497360715436428427');
                done();
            });
        });

        it('should call the passed in callback with User object',function(done) {
            var callback = function(err,data) {
                // pass
            }
            var callbackSpy = sinon.spy(callback);

            userInstance.getInfo(callbackSpy).then(function(retValue) {
                expect(retValue).to.be.instanceof(User);
                expect(retValue._info).to.deep.equal(responseData);
                expect(callbackSpy).to.be.calledOnce;
                expect(callbackSpy).to.be.calledWith(null,userInstance);
                done();
            });
        });
    });

    describe('_setAccessors()',function() {

        it('should set the accessors on the User instance',function(done) {
            userInstance.getInfo().then(function(retValue) {
                expect(userInstance.created_on).to.be.equal("2015-06-17T17:05:37.741005");
                expect(userInstance.is_server_owner).to.be.false;
                expect(userInstance.password).to.be.equal("$6$rounds=96671$qHl/Gpb.BrSqko8c$/mBbW.Iv4AGoxWvW95puHWkFTvD7UDl2DzrSQmQ7HlTe2FjUkKantkaJxiq0dfA4qrCIIeORGgeHGnfXN9GiO.");
                expect(userInstance.display_name).to.be.equal('black-perl');
                expect(userInstance.self_link).to.be.equal('http://localhost:8001/3.0/users/149132891060733238619497360715436428427');
                done();
            });
        });

        it('should set the display_name of the user',function(done) {
            userInstance.getInfo().then(function() {
                userInstance.display_name = 'johndoe';
                expect(userInstance.display_name).to.be.equal('johndoe');
                done();
            });
        });

        it('should set the plain text password',function(done) {
            userInstance.getInfo().then(function() {
                userInstance.password = 'unguessable';
                expect(userInstance._cleartext_password).to.be.equal('unguessable');
                done();
            });
        });
    }); // _setAccessors()

    describe('.preferences',function(done) {

        it('should return the preferences object corresponding to the user',function(done) {
            userInstance.getInfo().then(function() {
                var preferencesObj = userInstance.preferences;
                expect(preferencesObj).to.be.instanceof(Preferences);
                expect(preferencesObj._renderURI()).to.be.equal('http://localhost:8001/3.0/users/149132891060733238619497360715436428427/preferences');
                done();
            });
        });
    }); // .preferences

    describe('.addresses',function(done) {

        it('should return the addresses object corresponding to the user',function(done) {
            userInstance.getInfo().then(function() {
                var addressesObj = userInstance.addresses;
                expect(addressesObj).to.be.instanceof(Addresses);
                expect(addressesObj._userId).to.be.equal('149132891060733238619497360715436428427');
                done();
            });
        });
    }); // .addresses

    describe('.addAddress()',function(done) {

        it('should link an address to the User',function(done) {
            var email = 'testemail@gmail.com';
            var httpMethodSpy = sinon.spy(userInstance,'post');
            userInstance.addAddress(email).then(function(retValue) {
                expect(userInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/users/149132891060733238619497360715436428427/addresses');
                expect(httpMethodSpy).to.have.been.calledOnce;
                expect(httpMethodSpy).to.have.been.calledWith({email : email});
                expect(retValue).to.be.equal(userInstance);
                done();
            });
        });
    }); // .addAddress()

    describe('.getSubscriptions()',function(done) {
        var subscriptions = [
            { _options: 
               { endpoint: 'http://localhost:8001/3.0',
                 username: 'restadmin',
                 password: 'restpass',
                 forceAuth: true },
              _url: 'http://localhost:8001/3.0/members/182708720226748139525761445266416568295',
              _supportedMethods: [ 'get', 'delete' ],
              _template: '',
              _info: null,
              _preferences: null,
              _keys: [ 'email', 'role', 'list_id', 'self_link' ] },
            { _options: 
               { endpoint: 'http://localhost:8001/3.0',
                 username: 'restadmin',
                 password: 'restpass',
                 forceAuth: true },
              _url: 'http://localhost:8001/3.0/members/152676618417401827803610512929983878275',
              _supportedMethods: [ 'get', 'delete' ],
              _template: '',
              _info: null,
              _preferences: null,
              _keys: [ 'email', 'role', 'list_id', 'self_link' ] },
            { _options: 
               { endpoint: 'http://localhost:8001/3.0',
                 username: 'restadmin',
                 password: 'restpass',
                 forceAuth: true },
              _url: 'http://localhost:8001/3.0/members/323005600841240737688921431050789873980',
              _supportedMethods: [ 'get', 'delete' ],
              _template: '',
              _info: null,
              _preferences: null,
              _keys: [ 'email', 'role', 'list_id', 'self_link' ] },
            { _options: 
               { endpoint: 'http://localhost:8001/3.0',
                 username: 'restadmin',
                 password: 'restpass',
                 forceAuth: true },
              _url: 'http://localhost:8001/3.0/members/229727416505750597725746288488312764584',
              _supportedMethods: [ 'get', 'delete' ],
              _template: '',
              _info: null,
              _preferences: null,
              _keys: [ 'email', 'role', 'list_id', 'self_link' ] }
        ];

        it('should return the subscriptions corresponding to the user',function(done) {
            userInstance.getSubscriptions().then(function(retValue) {
                var ctr = 0;
                retValue.forEach(function(member) {
                    expect(member).to.be.instanceof(Member);
                    expect(member._url).to.be.equal(subscriptions[ctr++]._url);
                });
                done();
            });
        });
    }); // .getSubscriptions()

    describe('.getSubscriptionListIds()',function(done) {
        it('should returned the subscribed list ids\'',function(done) {
            userInstance.getSubscriptionListIds().then(function(retValue) {
                expect(retValue).to.be.deep.equal([ 'list_2.example1.org','list_2.example1.org','list_3.example2.org',
                'list_4.example2.org' ]);
                done();
            });
        });
    }); // .getSubscriptionListIds()

    describe('.save()',function(done) {
        it('should save the user properties',function(done) {
            var httpMethodSpy = sinon.spy(userInstance,'patch');
            userInstance.getInfo().then(function() {
                userInstance.display_name = 'johnsmith';
                userInstance.password = 'youcannotguess';
                userInstance.save().then(function(){
                    expect(userInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/users/149132891060733238619497360715436428427');
                    expect(httpMethodSpy).to.be.calledOnce;
                    expect(httpMethodSpy).to.have.been.calledWith({display_name : 'johnsmith',cleartext_password : 'youcannotguess'});
                    done();
                }); // .save()
            }); // .getInfo()
        });
    }); // .save() -- describe call

    describe('.deleteUser()',function(done) {
        it('should delete the user',function(done) {
            var httpMethodSpy = sinon.spy(userInstance,'delete');
            userInstance.deleteUser().then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(userInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/users/149132891060733238619497360715436428427');
                done();
            }); 
        });
    }); // .delete()

}); // User