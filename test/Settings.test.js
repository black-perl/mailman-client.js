'use strict';

/* 
Unit tests for the Member module
*/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Settings = require('../lib/Settings.js');

// nock.recorder.rec();
// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/settings.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('Settings',function() {
    var settingsInstance, options, responseData, responseKeys, LIST_READ_ONLY_ATTRS;

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    responseData = { 
      first_strip_reply_to: false,
      digest_size_threshold: 30,
      posting_pipeline: 'default-posting-pipeline',
      next_digest_number: 1,
      scheme: 'http',
      subscription_policy: 'moderate',
      last_post_at: null,
      autoresponse_postings_text: '',
      autorespond_owner: 'none',
      request_address: 'foo_list-request@somedomain.org',
      bounces_address: 'foo_list-bounces@somedomain.org',
      created_at: '2015-06-17T17:06:21.155428',
      leave_address: 'foo_list-leave@somedomain.org',
      admin_immed_notify: true,
      mail_host: 'somedomain.org',
      autorespond_requests: 'none',
      acceptable_aliases: [],
      http_etag: '"286754187f4dfc4b4c6ac3c316f6a7e4e21fb1c3"',
      web_host: 'somedomain.org',
      reply_goes_to_list: 'no_munging',
      default_member_action: 'defer',
      display_name: 'List_2',
      filter_content: false,
      archive_policy: 'public',
      posting_address: 'foo_list@somedomain.org',
      default_nonmember_action: 'hold',
      reply_to_address: '',
      send_welcome_message: true,
      join_address: 'foo_list-join@somedomain.org',
      autorespond_postings: 'none',
      subject_prefix: '[List_2] ',
      include_rfc2369_headers: true,
      owner_address: 'foo_list-owner@somedomain.org',
      autoresponse_grace_period: '90d',
      administrivia: true,
      description: 'List 2 for testing purposes',
      anonymous_list: false,
      volume: 1,
      list_name: 'foo_list',
      allow_list_posts: true,
      collapse_alternatives: true,
      convert_html_to_plaintext: false,
      welcome_message_uri: 'mailman:///welcome.txt',
      fqdn_listname: 'foo_list@somedomain.org',
      autoresponse_request_text: '',
      digest_last_sent_at: null,
      post_id: 1,
      advertised: true,
      no_reply_address: 'noreply@somedomain.org',
      admin_notify_mchanges: false,
      autoresponse_owner_text: '' 
    };

    responseKeys = [ 
      'first_strip_reply_to',
      'digest_size_threshold',
      'posting_pipeline',
      'next_digest_number',
      'scheme',
      'subscription_policy',
      'last_post_at',
      'autoresponse_postings_text',
      'autorespond_owner',
      'request_address',
      'bounces_address',
      'created_at',
      'leave_address',
      'admin_immed_notify',
      'mail_host',
      'autorespond_requests',
      'acceptable_aliases',
      'http_etag',
      'web_host',
      'reply_goes_to_list',
      'default_member_action',
      'display_name',
      'filter_content',
      'archive_policy',
      'posting_address',
      'default_nonmember_action',
      'reply_to_address',
      'send_welcome_message',
      'join_address',
      'autorespond_postings',
      'subject_prefix',
      'include_rfc2369_headers',
      'owner_address',
      'autoresponse_grace_period',
      'administrivia',
      'description',
      'anonymous_list',
      'volume',
      'list_name',
      'allow_list_posts',
      'collapse_alternatives',
      'convert_html_to_plaintext',
      'welcome_message_uri',
      'fqdn_listname',
      'autoresponse_request_text',
      'digest_last_sent_at',
      'post_id',
      'advertised',
      'no_reply_address',
      'admin_notify_mchanges',
      'autoresponse_owner_text' 
    ];

    LIST_READ_ONLY_ATTRS = [
    'bounces_address',
    'created_at',
    'digest_last_sent_at',
    'fqdn_listname',
    'http_etag',
    'join_address',
    'last_post_at',
    'leave_address',
    'list_id',
    'list_name',
    'mail_host',
    'next_digest_number',
    'no_reply_address',
    'owner_address',
    'post_id',
    'posting_address',
    'request_address',
    'scheme',
    'volume',
    'web_host',
    ];

    beforeEach(function() {
        settingsInstance = new Settings(options,
                                        '/lists/foo_list@somedomain.org/config');
    });

    describe('constructor',function() {

        it('should be an instance of BaseRequest',function() {
            expect(settingsInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of ApiRequest',function() {
            expect(settingsInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of Settings',function() {
            expect(settingsInstance).to.be.instanceof(Settings);
        });

        it('should intialise the instance properties',function() {
            expect(settingsInstance._supportedMethods).to.be.deep.equal(['get','patch']);
            expect(settingsInstance._template).to.be.equal('/lists/foo_list@somedomain.org/config');
        });

    }); // constructor

    describe('.getInfo()',function(done) {

        it('_.renderURI()',function(done) {
            settingsInstance.getInfo().then(function() {
                expect(settingsInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/foo_list@somedomain.org/config');
                done();
            });
        });

        it('should call the passed in callback instance with the Settings instance',function(done) {
            var callback = function(err,data) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            settingsInstance.getInfo(callbackSpy).then(function() {
                expect(callbackSpy).to.be.calledOnce;
                expect(callbackSpy).to.be.calledWith(null,settingsInstance);
                expect(settingsInstance._info).to.be.deep.equal(responseData);
                done();
            });
        });

        it('should return a promise to the settingsInstance',function(done) {
            settingsInstance.getInfo().then(function(retValue) {
                expect(retValue).to.be.deep.equal(settingsInstance);
                expect(settingsInstance._info).to.be.deep.equal(responseData);
                done();
            });
        });

    }); //.getInfo()

    describe('.getKeys()',function(done) {

        it('should return the settings keys',function(done){
            settingsInstance.getKeys().then(function(keysArray) {
                expect(keysArray.sort()).to.be.deep.equal(responseKeys.sort());
                done();
            });
        });

    }); // .getKeys()

    describe('.getValue()',function(done) {
        
        it('should return the value corresponding to a key',function(done) {
            var loopCount, counter = 0;
            settingsInstance.getKeys().then(function(keys) {
                loopCount = keys.length;
                keys.forEach(function(key) {
                    settingsInstance.getValue(key,null).then(function(value) {
                        if ( key in responseData )
                            expect(value).to.be.deep.equal(responseData[key]);
                        else
                            expect(value).to.be.equal(null);
                        if ( ++counter === loopCount ) {
                            done();
                        }
                    });
                });
            });
        });

    }); // .getValue()

    describe('._setAccessors()',function(done) {

        it('should set the accessors on the Settings instance',function(done) {
            settingsInstance.getInfo().then(function() {
                expect(settingsInstance.list_name).to.be.equal('foo_list');
                // set the list name
                settingsInstance.list_name = 'myawesomelist';
                expect(settingsInstance.list_name).to.be.equal('myawesomelist');
                done();
            });
        });

    }); // .setAccessors()

    describe('.save()',function(done) {

        it('should save the settings upstream',function(done) {
            var httpMethodSpy = sinon.spy(settingsInstance,'patch');
            settingsInstance.getInfo().then(function() {
                return settingsInstance.save();
            }).then(function(retValue) {
                expect(retValue).to.be.deep.equal(settingsInstance);
                expect(settingsInstance._info).to.be.null;
                expect(httpMethodSpy).to.be.calledOnce;
                var keys = Object.keys(httpMethodSpy.getCall(0).args[0]);
                keys.forEach(function(key) {
                    expect(LIST_READ_ONLY_ATTRS).not.to.have.any.keys(key);
                });
                done();
            });
        });

    }); // .save()

    describe('.toString()',function() {

        it('should have a string representation',function(done) {
            settingsInstance.getInfo().then(function() {
                expect(settingsInstance.toString()).to.be.deep.equal(JSON.stringify(settingsInstance._info));
                done();
            });
        });

    }); // .toString()

}); // Settings