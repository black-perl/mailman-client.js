'use strict';

/**
* Unit tests for the Address module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Address = require('../lib/Address.js');
var Preferences = require('../lib/Preferences.js');

// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/address.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('Address',function() {
    var addrRequest, address, options; 

    address = {
               registered_on : "2015-06-18T06:42:21.623055",
               user : "http://localhost:8001/3.0/users/e37d8c4c160be1f1f5e57e9f6f05e7984235949c",
               self_link : "http://localhost:8001/3.0/addresses/johndoe@mailman.com",
               email : "johndoe@mailman.com",
               original_email : "johndoe@mailman.com",
               http_etag : "\"e37d8c4c160be1f1f5e57e9f6f05e7984235949c\""
              };
    options = { 
               endpoint :'http://localhost:8001/3.0',
               username : 'restadmin',
               password : 'restpass' 
              };

    beforeEach(function() {
        addrRequest = new Address(options,address);
    });

    describe('constructor',function() {

        it('should be an instance of ApiRequest',function() {
            expect(addrRequest).to.be.instanceof(ApiRequest);
        });

        it('should be an instace of BaseRequest',function() {
            expect(addrRequest).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of Address',function() {
            expect(addrRequest).to.be.instanceof(Address);
        });

        it('should initialise the instance properties',function() {
            expect(addrRequest._supportedMethods).to.be.deep.equal(['get','post']);
            expect(addrRequest._options).to.be.deep.equal(options);
            expect(addrRequest._url).to.be.equal(address.self_link);
            expect(addrRequest.email).to.be.equal(address.email);
        });

    }); // constructor

    describe('.getInfo()',function() {

        var responseData = {  
                            original_email: 'johndoe@mailman.com',
                            email: 'johndoe@mailman.com',
                            http_etag: '"e37d8c4c160be1f1f5e57e9f6f05e7984235949c"',
                            self_link: 'http://localhost:8001/3.0/addresses/johndoe@mailman.com',
                            registered_on: '2015-06-18T06:42:21.623055',
                            user: 'http://localhost:8001/3.0/users/97071403524618786289886643396072006536' 
                           };
                           
        it('should call the passed in callback function with the addrRequest object',function(done) {
            var callback = function(err,retValue) {
                // pass
            }
            var callbackSpy = sinon.spy(callback);

            addrRequest.getInfo(callbackSpy).then( function() {
                expect(callbackSpy).to.have.been.calledOnce;
                expect(callbackSpy).to.have.been.calledWith(null,addrRequest);
                expect(addrRequest._info).to.deep.equal(responseData);
                done();
            });
        });

        it('should return a promise to the addrRequest object',function(done){
            addrRequest.getInfo().then(function(retValue) {
                expect(retValue).to.deep.equal(addrRequest);
                expect(addrRequest._info).to.deep.equal(responseData);
                done();
            });
        });

    }); // .getInfo()

    describe('._setAccessors()',function() {

        it('should set the accessors on the Address instance properties',function(done){
            addrRequest.getInfo().then(function(retValue) {
                expect(addrRequest.registered_on).to.be.equal('2015-06-18T06:42:21.623055');
                expect(addrRequest.user).to.be.equal('http://localhost:8001/3.0/users/97071403524618786289886643396072006536');
                expect(addrRequest.email).to.be.equal('johndoe@mailman.com');
                done();
            });
        });

    }); // .setAccessors()

    describe('.preferences',function() {

        it('should return a Preferences instance',function(done) {
            addrRequest.getInfo().then(function() {
                expect(addrRequest.preferences).to.be.instanceof(Preferences);
                done();
            });
        });

    }); // preferences

    describe('.unverify()',function() {

        it('takes a callback function',function(done) {
            var callback = function(err,retData) {
                // pass
            }
            var callbackSpy = sinon.spy(callback);

            addrRequest.unverify(callbackSpy).then(function(retValue) {
                expect(callbackSpy).to.have.been.calledOnce;
                expect(callbackSpy).to.have.been.calledWith(null,addrRequest);
                done();
            });
        });

    }); // .unverify()


    describe('.verify()',function() {

        it('should verify the email address',function(done) {
            var callback = function(err,Value) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            // no error means request is successful
            addrRequest.verify(callbackSpy).then(function(retValue) {
                expect(callbackSpy).to.have.been.calledOnce;
                expect(callbackSpy).to.have.been.calledWith(null,addrRequest);
                done();
            });
        });

    }); //  .verify()

});