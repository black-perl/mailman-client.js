'use strict';

/**
Unit tests for the Page module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var List = require('../lib/List.js');
var Page = require('../lib/Page.js');


// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/page.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('Page',function() {
    var pageInstance, options, responseData;

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    responseData =  { 
        start: 0,
        http_etag: '"f4905cd1401fc3eb9f8cfba5617b075f9b1591df"',
        entries: 
        [ { self_link: 'http://localhost:8001/3.0/lists/foolist.somedomain.org',
           volume: 1,
           list_id: 'foolist.somedomain.org',
           http_etag: '"87b1483f06458ed08fb31c309a7b57ce72fdae36"',
           member_count: 0,
           fqdn_listname: 'foolist@somedomain.org',
           list_name: 'foolist',
           display_name: 'FooList',
           mail_host: 'somedomain.org' },
         { self_link: 'http://localhost:8001/3.0/lists/foobarlist.somedomain1.org',
           volume: 1,
           list_id: 'foobarlist.somedomain1.org',
           http_etag: '"ba516f01a494004646cea53cebc1d3091230513b"',
           member_count: 0,
           fqdn_listname: 'foobarlist@somedomain1.org',
           list_name: 'foobarlist',
           display_name: 'FooBarList',
           mail_host: 'somedomain1.org' },
         { self_link: 'http://localhost:8001/3.0/lists/dummylist.somedomain.org',
           volume: 1,
           list_id: 'dummylist.somedomain.org',
           http_etag: '"b804d45faa77a7418a2a9e03030cc5a29becd306"',
           member_count: 0,
           fqdn_listname: 'dummylist@somedomain.org',
           list_name: 'dummylist',
           display_name: 'Dummy_List',
           mail_host: 'somedomain.org' } ],
        total_size: 3 
    };


    beforeEach(function() {
        pageInstance = new Page(options,'/lists',List,3,1);
    });

    before('constructor',function() {

        it('should be an instance of ApiRequest',function() {
            expect(pageInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of BaseRequest',function() {
            expect(pageInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of Member',function() {
            expect(pageInstance).to.be.instanceof(Member);
        });

        it('should initialise the instance properties',function() {
            expect(pageInstance._supportedMethods).to.be.deep.equal(['get']);
            expect(pageInstance._template).to.be.equal('/lists');
            expect(pageInstance._model).to.be.deep.equal(List);
            expect(pageInstance._count).to.be.deep.equal(3);
            expect(pageInstance._page).to.be.deep.equal(1);
        });

    }); // constructor

    describe('._createPage',function(done) {

        it('should call the callback function with the Page instance',function(done) {
            var callback = function(err,data) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            var querySpy = sinon.spy(pageInstance,'query');

            pageInstance._createPage(callbackSpy).then(function() {
                expect(querySpy).to.be.calledOnce;
                expect(querySpy).to.be.calledWith({count : '3', page : '1'});
                expect(callbackSpy).to.be.calledOnce;
                var callargs = callbackSpy.getCall(0).args[1]; // calling arguments of the callback spy
                expect(callargs).to.be.deep.equal(pageInstance);
                var counter = 0;
                pageInstance._entries.forEach(function(arg) {
                    expect(arg).to.be.instanceof(List);
                    expect(arg._url).to.be.equal(responseData.entries[counter++].self_link);
                });
                done();
            }); // ._createPage()
        });

        it('should provide promise to the Page instance',function(done) {
            var querySpy = sinon.spy(pageInstance,'query');
            pageInstance._createPage().then(function(retValue) {
                expect(querySpy).to.be.calledOnce;
                expect(querySpy).to.be.calledWith({count : '3', page : '1'});
                expect(retValue).to.be.deep.equal(pageInstance);
                var counter = 0;
                pageInstance._entries.forEach(function(obj) {
                    expect(obj).to.be.instanceof(List);
                    expect(obj._url).to.be.equal(responseData.entries[counter++].self_link);
                });
                done();
            }); // ._createPage()
        });

    }); // ._createPage()

    describe('.previous()',function() {

        it('should provide the previous page',function() {
            var callback = function(err,data) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            pageInstance._page = 50; // set to a random value
            var currentPage = pageInstance._page;
            var createPageStub = sinon.stub(pageInstance,'_createPage',function(cb) { cb() });

            pageInstance.previous(callbackSpy);

            expect(pageInstance._page).to.be.equal(currentPage-1);
            expect(pageInstance._entries).to.be.null;
            expect(createPageStub).to.be.calledOnce;
            expect(createPageStub).to.be.calledWith(callbackSpy);
            expect(callbackSpy).to.be.calledOnce;
        });

    }); // .previous()

    describe('.next()',function() {

        it('should provide the next page',function() {
            var callback = function(err,data) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            pageInstance._page = 50; // set to a random value
            var currentPage = pageInstance._page;
            var createPageStub = sinon.stub(pageInstance,'_createPage',function(cb) { cb() });

            pageInstance.next(callbackSpy);

            expect(pageInstance._page).to.be.equal(currentPage+1);
            expect(pageInstance._entries).to.be.null;
            expect(createPageStub).to.be.calledOnce;
            expect(createPageStub).to.be.calledWith(callbackSpy);
            expect(callbackSpy).to.be.calledOnce;
        });

    }); // .next()

    describe('.current()',function(done) {

        it('should provide the current page',function(done) {
            pageInstance._createPage().then(function() {
                var callback = function(err,data) {
                    // pass
                };
                var callbackSpy = sinon.spy(callback);
                pageInstance._page = 50; // set to a random value
                var currentPage = pageInstance._page;
                var createPageStub = sinon.stub(pageInstance,'_createPage',function(cb) { cb() });

                pageInstance.current(callbackSpy);

                expect(pageInstance._page).to.be.equal(currentPage);
                expect(pageInstance._entries).not.to.be.null;
                expect(createPageStub).to.be.calledOnce;
                expect(createPageStub).to.be.calledWith(callbackSpy);
                expect(callbackSpy).to.be.calledOnce;
                done();
            });
        });

    }); // .current()

    describe('.each()',function(done) {

        it('should call the callback function with each page entry',function(done) {
            var callback = function(entry) {
                expect(entry).to.be.instanceof(List);
                expect(entry._url).to.be.equal(responseData.entries[counter++].self_link);
                if ( counter == 3 ) {
                    done();
                }
            };
            var callbackSpy = sinon.spy(callback);
            var counter = 0;

            pageInstance.each(callbackSpy);

        });

    }); // .each()

    describe('.all()',function(done) {

        it('should return a promise to the Page entries',function(done) {
            pageInstance.all().then(function(entries) {
                expect(entries).to.be.deep.equal(pageInstance._entries);
                done();
            }); // .all()
        }); 

    });

});
