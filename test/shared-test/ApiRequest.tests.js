'use strict';

/**
* Unit tests for the ApiRequest module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));

var ApiRequest = require('../../lib/shared/ApiRequest.js');
var BaseRequest = require('../../lib/shared/BaseRequest.js');

describe('ApiRequest',function() {
    var apiRequest;

    beforeEach(function() {
        apiRequest = new ApiRequest({'endpoint' : 'http://black-perl.me'});
    });

    describe('Constructor',function() {

        it('should be an instance of ApiRequest', function() {
            expect(apiRequest).to.be.instanceof(ApiRequest);
        });

        it('should inherit from BaseRequest',function() {
            expect(apiRequest).to.be.instanceof(BaseRequest);
        });

        it('should initialise the instance properties',function() {
            expect(apiRequest._options).to.deep.equal({ endpoint:'http://black-perl.me' });
            expect(apiRequest._params).to.deep.equal({});
            expect(apiRequest._queryParams).to.be.false;
        });

    }); // constructor

    describe('.query()',function() {

        it('should set _params when both key,value pair are strings',function() {
            apiRequest.query('name','anonymous');

            expect(apiRequest._params).to.have.property('name');
            expect(apiRequest._params.name).to.equal('anonymous');
        });

        it('should set _params when both key is string and value is array',function() {
            apiRequest.query('numbers',[1,2,3,4,5]);

            expect(apiRequest._params).to.have.property('numbers');
            expect(apiRequest._params.numbers).to.deep.equal([1,2,3,4,5]);
        });

        it('should set _params when only a key object is passed',function() {
            apiRequest.query({key1 : 'val1',key2 : 'val2'});

            expect(apiRequest._params).to.have.property('key1');
            expect(apiRequest._params.key1).to.equal('val1');
            expect(apiRequest._params).to.have.property('key2');
            expect(apiRequest._params.key2).to.equal('val2');
        });

        it('should throw an error if proper arguments are not passed in',function() {
            expect( apiRequest.query.bind(apiRequest,{key1 : 'val1',key2 : 'val2'},'something') ).to.throw('Arguments not found in the required form.');
            expect( apiRequest.query.bind(apiRequest,{5:5}) ).to.throw('Unsupported object supplied, keys should be of type String and Value should be of type String/Array.');
        });

        it('should set _queryParams to be true', function() {
            apiRequest.query('key','val');

            expect(apiRequest._queryParams).to.be.true;
        });
    }); // .query()

    describe('_renderQuery()',function() {

        it('should properly render a query string by using _params',function() {
            apiRequest.query('key1','value1')
                      .query('key2',[1,2,3])
                      .query({ key3 : 'value3',key4 : ['a','b','c'] });

            expect(apiRequest._renderQuery()).to.equal('key1=value1&key2%5B0%5D=1&key2%5B1%5D=2&key2%5B2%5D=3&key3=value3&key4%5B0%5D=a&key4%5B1%5D=b&key4%5B2%5D=c');
        });

    }); // ._renderQuery()
});
