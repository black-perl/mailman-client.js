/* Mock the superagent module for testing */
'use strict';

var SuperAgentMock = function() {
    /* pass */
}

/* dummy function */
function noop() { 
    return;
}

SuperAgentMock.prototype = {
    get : function() {
        return this;
    },

    post : function() {
        return this;
    },

    put : function() {
        return this;
    },

    patch : function() {
        return this;
    },

    del : function() {
        return this;
    },

    head : function() {
        return this;
    },

    send : function(data) {
        this._data = data;
        return this;
    },

    auth : function() {
        return this;
    },

    type : function() {
        return this;
    },

    end : function(callback) {
        // this._error or this._response to replicate the effect of error & response respectively
        callback = callback || noop;
        // return empty object if none of the above two is set
        callback(this._error,this._response || {});
    }

}

module.exports = SuperAgentMock;