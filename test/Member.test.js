'use strict';

/** 
Unit tests for the Member module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Preferences = require('../lib/Preferences.js');
var User = require('../lib/User.js');
var Member = require('../lib/Member.js');


// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/member.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}


describe('Member',function() {
    var memberInstance,options,responseData;

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    responseData = { 
        email: 'johndoe@gmail.com',
        delivery_mode: 'regular',
        member_id: 251966682821025451828522645794687675727,
        list_id: 'example_list.domain.org',
        user: 'http://localhost:8001/3.0/users/133826164605064147732877946476680872392',
        http_etag: '"a0f7c3a52f5f444e52d8cd782a77972909302d04"',
        self_link: 'http://localhost:8001/3.0/members/251966682821025451828522645794687675727',
        address: 'http://localhost:8001/3.0/addresses/johndoe@gmail.com',
        role: 'owner' 
    };

    beforeEach(function() {
        memberInstance = new Member(options,
                                    "http://localhost:8001/3.0/members/251966682821025451828522645794687675727");
    });

    describe('constructor',function() {

        it('should be an instance of ApiRequest',function() {
            expect(memberInstance).to.be.instanceof(ApiRequest);
        });

        it('should be an instance of BaseRequest',function() {
            expect(memberInstance).to.be.instanceof(BaseRequest);
        });

        it('should be an instance of Member',function() {
            expect(memberInstance).to.be.instanceof(Member);
        });

        it('should initialise the instance properties',function() {
            expect(memberInstance._supportedMethods).to.be.deep.equal(['get','delete']);
            expect(memberInstance._url).to.be.equal("http://localhost:8001/3.0/members/251966682821025451828522645794687675727");
        });

    }); // constructor

    describe('.getInfo()',function(done) {

        it('._renderURI()',function(done) {
            memberInstance.getInfo().then(function() {
                expect(memberInstance._renderURI()).to.be.equal("http://localhost:8001/3.0/members/251966682821025451828522645794687675727");
                done();
            });
        });

        it('should call the passed in callback function with the Member instance',function(done) {
            var callback = function(err,data) {
                // pass
            };
            var callbackSpy = sinon.spy(callback);
            memberInstance.getInfo(callbackSpy).then(function() {
                expect(callbackSpy).to.be.calledOnce;
                expect(callbackSpy).to.be.calledWith(null,memberInstance);
                expect(memberInstance._info).to.be.deep.equal(responseData);
                done();
            });
        });

        it('should provide a promise to the Member instance',function(done) {
            memberInstance.getInfo().then(function(retValue) {
                expect(retValue).to.be.deep.equal(memberInstance);
                expect(memberInstance._info).to.be.deep.equal(responseData);
                done();
            });
        });

    }); // .getInfo()

    describe('._setAccessors()',function(done) {

        it('should set the accessors on the Member instance',function(done) {
            memberInstance.getInfo().then(function() {
                expect(memberInstance.email).to.be.equal('johndoe@gmail.com');
                expect(memberInstance.role).to.be.equal('owner');
                expect(memberInstance.list_id).to.be.equal('example_list.domain.org');
                expect(memberInstance.self_link).to.be.equal('http://localhost:8001/3.0/members/251966682821025451828522645794687675727');
                done();
            });
        });

    }); // ._setAccessors()

    describe('.preferences',function(done) {

        it('should return the Preferences object corresponding to the Member instance',function(done) {
            memberInstance.getInfo().then(function() {
                var preferencesObj = memberInstance.preferences;
                expect(preferencesObj).to.be.instanceof(Preferences);
                expect(preferencesObj._renderURI()).to.be.equal('http://localhost:8001/3.0/members/251966682821025451828522645794687675727/preferences');
                done();
            });
        });

    }); // .preferences

    describe('.user',function(done) {

        it('should return the User object corresponding to the Member instance',function(done) {
            memberInstance.getInfo().then(function() {
                var userObj = memberInstance.user;
                expect(userObj).to.be.instanceof(User);
                expect(userObj._rootTemplate).to.be.equal('/users/133826164605064147732877946476680872392');
                done();
            });
        });

    }); // .user

    describe('.unsubscribe()',function(done) {

        it('should unsubscribe the Member from the list',function(done) {
            var httpMethodSpy = sinon.spy(memberInstance,'delete');
            memberInstance.unsubscribe().then(function() {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(memberInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/members/251966682821025451828522645794687675727');
                done();
            });
        });
        
    }); // .unsubscribe()

});
