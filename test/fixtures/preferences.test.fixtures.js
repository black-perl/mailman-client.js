var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/system/preferences')
  .times(7)
  .reply(200, {"delivery_mode":"regular","receive_list_copy":true,"http_etag":"\"e93578021ad619b9f6a8a5ce2f9f00198e5eb53d\"","receive_own_postings":true,"preferred_language":"en","acknowledge_posts":false,"delivery_status":"enabled","hide_address":true,"self_link":"http://localhost:8001/3.0/system/preferences"}, { date: 'Thu, 30 Jul 2015 16:19:29 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '315',
  'content-type': 'application/json; charset=utf-8' });
