var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/lists/foolist.somedomain.org/archivers')
  .times(7)
  .reply(200, {"mhonarc":false,"mail-archive":false,"http_etag":"\"e8abfb237f8ebda71c95d59b907288473359bb5a\"","prototype":false}, { date: 'Fri, 31 Jul 2015 17:29:01 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '122' });

nock('http://localhost:8001')
  .put('/3.0/lists/foolist.somedomain.org/archivers', "mhonarc=true&mail-archive=false&prototype=false")
  .reply(204, "", { date: 'Fri, 31 Jul 2015 17:29:01 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

