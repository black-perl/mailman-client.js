var nock = require('nock');

nock('http://localhost:8001')
  .get('/3.0/addresses/johndoe@mailman.com')
  .times(6)
  .reply(200, {"original_email":"johndoe@mailman.com","email":"johndoe@mailman.com","http_etag":"\"e37d8c4c160be1f1f5e57e9f6f05e7984235949c\"","self_link":"http://localhost:8001/3.0/addresses/johndoe@mailman.com","registered_on":"2015-06-18T06:42:21.623055","user":"http://localhost:8001/3.0/users/97071403524618786289886643396072006536"}, { date: 'Tue, 23 Jun 2015 07:16:55 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-type': 'application/json; charset=utf-8',
  'content-length': '371' });

nock('http://localhost:8001')
  .post('/3.0/addresses/johndoe@mailman.com/verify', {})
  .reply(204, "", { date: 'Tue, 23 Jun 2015 10:03:15 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });

nock('http://localhost:8001')
  .post('/3.0/addresses/johndoe@mailman.com/unverify', {})
  .reply(204, "", { date: 'Tue, 23 Jun 2015 10:03:15 GMT',
  server: 'WSGIServer/0.2 CPython/3.4.0',
  'content-length': '0' });