'use strict';

/**
Unit tests for the Client module
**/

var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
chai.use(require('sinon-chai'));
var nock = require('nock');
var path = require('path');

var ApiRequest = require('../lib/shared/ApiRequest.js');
var BaseRequest = require('../lib/shared/BaseRequest.js');
var Preferences = require('../lib/Preferences.js');
var User = require('../lib/User.js');
var Domain = require('../lib/Domain.js');
var Address = require('../lib/Address.js');
var List = require('../lib/List.js');
var Member = require('../lib/Member.js');
var Page = require('../lib/Page.js');
var Client = require('../lib/Client.js');


// nock.recorder.rec();
// loading fixtures
var fixturesPath = path.join(__dirname,'fixtures/client.test.fixtures.js');
try {
    require(fixturesPath);
} catch(e) {
    console.log(e);
    throw new Error('Fixtures file not found');
}

describe('Client',function() {
    var clientInstance, options;

    options = {
        endpoint : 'http://localhost:8001/3.0',
        username : 'restadmin',
        password : 'restpass'
    };

    beforeEach(function() {
        clientInstance = new Client(options);
    });

    describe('constructor',function() {

        it('should be an instance of BaseRequest',function() {
            expect(clientInstance).to.be.an.instanceof(BaseRequest);
        });

        it('should be an instance of ApiRequest',function() {
            expect(clientInstance).to.be.an.instanceof(ApiRequest);
        });

        it('should be an instance of Client',function() {
            expect(clientInstance).to.be.an.instanceof(Client);
        }); 

    }); // constructor

    describe('.getVersions()',function(done) {

        it('should provide the version details for the mailman core',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getVersions().then(function() {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/system/versions');
                expect(httpMethodSpy).to.be.calledOnce;
                done();
            });
        });

    }); // .getVersions()

    describe('.preferences',function() {

        it('should return a Preferences instance',function() {
            var preferencesInstance = clientInstance.preferences;
            expect(preferencesInstance).to.be.instanceof(Preferences);
            expect(preferencesInstance._template).to.be.equal('/system/preferences');
        });

    }); // .preferences

    describe('.getLists()',function(done) {

        it('should provide promise to all the lists',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getLists().then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists');
                expect(httpMethodSpy).to.be.calledOnce;
                retValue.forEach(function(entry) {
                    expect(entry).to.be.an.instanceof(List);
                });
                done();
            });
        });

    }); // .getLists()

    describe('.getListPage()',function() {

        it('should return a Page instance corresponding to lists',function() {
            var page = clientInstance.getListPage(50,1);
            expect(page).to.be.an.instanceof(Page);
            expect(page._model).to.be.deep.equal(List);
            expect(page._count).to.be.equal(50);
            expect(page._page).to.be.equal(1);
        });

    }); // .listPage()

    describe('.getDomains()',function() {

        it('should return promise to all the domains',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getDomains().then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/domains');
                expect(httpMethodSpy).to.be.calledOnce;
                retValue.forEach(function(entry) {
                    expect(entry).to.be.an.instanceof(Domain);
                });
                done();
            });
        });

    }); // .getDomains()

    describe('.getMembers()',function(done) {

        it('should return promise to all the members',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getMembers().then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/members');
                expect(httpMethodSpy).to.be.calledOnce;
                retValue.forEach(function(entry) {
                    expect(entry).to.be.an.instanceof(Member);
                });
                done();
            });
        });

    }); // .getMembers()

    describe('.getMemberPage()',function() {

        it('should return a Page instance corresponding to members',function() {
            var page = clientInstance.getMemberPage(50,1);
            expect(page).to.be.an.instanceof(Page);
            expect(page._model).to.be.deep.equal(Member);
            expect(page._count).to.be.equal(50);
            expect(page._page).to.be.equal(1);
        });

    }); // .memberPage()

    describe('.getUsers()',function(done) {

        it('should return promise to all the users',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getUsers().then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/users');
                expect(httpMethodSpy).to.be.calledOnce;
                retValue.forEach(function(entry) {
                    expect(entry).to.be.an.instanceof(User);
                });
                done();
            });
        });

    }); // .getUser()

    describe('.getUserPage()',function() {

        it('should return a Page instance corresponding to users',function() {
            var page = clientInstance.getUserPage(50,1);
            expect(page).to.be.an.instanceof(Page);
            expect(page._model).to.be.deep.equal(User);
            expect(page._count).to.be.equal(50);
            expect(page._page).to.be.equal(1);
        });

    }); // .userPage()

    describe('.createDomain()',function(done) {

        it('should create a domain',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'post');
            var payload = {
                mail_host : 'foobar.com',
                base_url : 'http://foobar.com',
                description : 'Foo bar',
                owner : 'bob@foomail.com'
            };
            clientInstance.createDomain('foobar.com','http://foobar.com','Foo bar','bob@foomail.com').then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/domains');
                expect(httpMethodSpy).to.be.calledOnce;
                expect(httpMethodSpy).to.be.calledWith(payload,null,'identity');
                expect(retValue).to.be.instanceof(Domain);
                expect(retValue._url).to.be.equal('http://localhost:8001/3.0/domains/foobar.com');
                done();
            }); 
        });

    }); // .createDomain()

    describe('.deleteDomain()',function(done) {

        it('should delete a domain',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'delete');
            clientInstance.deleteDomain('example2.org').then(function(retValue) {
                expect(retValue).to.be.an.instanceof(Client);
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/domains/example2.org');
                expect(httpMethodSpy).to.be.calledOnce;
                done();
            }); 
        });

    }); // .deleteDomain()

    describe('.getDomain()',function(done) {

        it('should get the Domain instance when mailhost is provided',function(done) {
            clientInstance.getDomain('example2.org').then(function(retValue) {
                expect(retValue).to.be.instanceof(Domain);
                expect(retValue._url).to.be.equal('http://localhost:8001/3.0/domains/example2.org');
                done();
            });
        });

        it('should get the Domain instance by the webhost is provided',function(done) {
            clientInstance.getDomain(null,'http://example2.org').then(function(retValue) {
                expect(retValue).to.be.instanceof(Domain);
                expect(retValue._url).to.be.equal('http://localhost:8001/3.0/domains/example2.org');
                done();
            }); 
        });

    }); // .getDomain()

    describe('.createUser()',function(done) {

        it('should create a user',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'post');
            clientInstance.createUser('bob@gmail.com','unguessable','bob frank').then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/users');
                expect(httpMethodSpy).to.be.calledOnce;
                var payload = {
                    email : 'bob@gmail.com',
                    password : 'unguessable',
                    display_name : 'bob frank'
                };
                expect(httpMethodSpy).to.be.calledWith(payload,null,'identity');
                expect(retValue).to.be.instanceof(User);
                done();
            });
        });

    }); // .createUser()

    describe('.getUser()',function(done) {

        it('should get the User instance corresponding to an email address',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getUser('bobwills@gmail.com').then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/users/bobwills@gmail.com');
                expect(httpMethodSpy).to.be.calledOnce;
                expect(retValue).to.be.instanceof(User);
                done();
            });
        });

    }); // .getUser()

    describe('.getAddress()',function(done) {

        it('should get the Address instance corresponding to an email address',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getAddress('johndoe@yahoo.com').then(function(retValue) {
                expect(httpMethodSpy).to.be.calledOnce;
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/addresses/johndoe@yahoo.com');
                expect(retValue).to.be.instanceof(Address);
                done();
            });
        });

    }); // ,getAddress()

    describe('.getList()',function(done) {

        it('should return an List instance corresponding to fqdn_listname',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'get');
            clientInstance.getList('list_2@example1.org').then(function(retValue) {
                expect(clientInstance._renderURI()).to.be.equal('http://localhost:8001/3.0/lists/list_2@example1.org');
                expect(httpMethodSpy).to.be.calledOnce;
                expect(retValue).to.be.instanceof(List);
                expect(retValue._url).to.be.equal('http://localhost:8001/3.0/lists/list_2.example1.org');
                done();
            })
        });

    }); // .getList()

    describe('.deleteList()',function(done) {

        it('should delete the list',function(done) {
            var httpMethodSpy = sinon.spy(clientInstance,'delete');
            clientInstance.deleteList('list_2@example1.org').then(function(retValue) {
                expect(retValue).to.be.deep.equal(clientInstance);
                expect(httpMethodSpy).to.be.calledOnce;
                done();
            });
        });

    }); // .deleteList()
 
}); // Client



